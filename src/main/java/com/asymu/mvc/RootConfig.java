package com.asymu.mvc;

import org.springframework.context.annotation.ComponentScan;

@ComponentScan("com.asymu.mvc")
public class RootConfig {

}
