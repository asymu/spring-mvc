package com.asymu.mvc;

import org.springframework.stereotype.Service;

@Service
public class GreetingService {
	
	public String greetMessage() {
		return "Hello Syed!";
	}

}
