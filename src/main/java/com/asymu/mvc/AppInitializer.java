//package com.asymu.mvc;
//
//import javax.servlet.ServletContext;
//import javax.servlet.ServletException;
//import javax.servlet.ServletRegistration;
//
//import org.springframework.web.WebApplicationInitializer;
//import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;
//import org.springframework.web.servlet.DispatcherServlet;
//  if we are using this we need to comment ExpeditedWayOfDispatcherInitializer class
//public class AppInitializer implements WebApplicationInitializer {
//
//	@Override
//	public void onStartup(ServletContext servletContext) throws ServletException {
//		// TODO Auto-generated method stub
//		System.out.println("Inside AppInitializer");
//		AnnotationConfigWebApplicationContext context = new AnnotationConfigWebApplicationContext();
//		context.register(WebConfig.class);
//		
//		DispatcherServlet dispatcherServlet = new DispatcherServlet(context);
//		ServletRegistration.Dynamic registration = servletContext.addServlet("dispatcher", dispatcherServlet);
//		registration.addMapping("/");//all requests
//		registration.setLoadOnStartup(1);
//		
//	}
//
//}
