package com.asymu.mvc;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.view.InternalResourceViewResolver;

@EnableWebMvc 
@ComponentScan("com.asymu.mvc.controllers") // discover all annotations in this package i.e., controller, component.. and turn them into bean
public class WebConfig {
	
	@Bean
	public InternalResourceViewResolver resourceViewResolver() {
		InternalResourceViewResolver viewResolverBean = new InternalResourceViewResolver();
		viewResolverBean.setPrefix("/WEB-INF/views/");
		viewResolverBean.setSuffix(".jsp");
		return viewResolverBean;
	}

}
