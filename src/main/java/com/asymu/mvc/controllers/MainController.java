package com.asymu.mvc.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.asymu.mvc.GreetingService;

@Controller
@RequestMapping("/main")
public class MainController {
	
	@Autowired
	private GreetingService service;

	/*
	 * @ResponseBody
	 * 
	 * @RequestMapping("/") public String method() { return
	 * this.service.greetMessage(); }
	 */
	
	
	@RequestMapping("/")
	public String method(Model model) {
		model.addAttribute("message", this.service.greetMessage());
		return "hello";
	}
	
}
